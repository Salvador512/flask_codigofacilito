from flask import Flask
from flask import request
app = Flask(__name__)

@app.route('/')
def index():
	return "Hola Mundo"

#http://127.0.0.1:8000/params
#http://127.0.0.1:8000/params?params1=Salvador
#http://127.0.0.1:8000/params?params1=Salvador&params2=Campanella
@app.route('/params')
def params():
	param = request.args.get('params1','no contiene este parametro')
	param_dos = request.args.get('params2','no contiene este parametro')
	return 'El parametro es : {} , {}'.format(param, param_dos)	

if __name__ == '__main__':
	app.run(debug=True,port=8000)