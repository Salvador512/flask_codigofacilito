# Curso de Flask - Por codigofacilito 

## Como Ejecutar

### Vitualenv
1. Abrir una terminal
2. Ejecutar `/src/Scripts/activate`

### Flask
Una vez el virtualenv este activado
`python archivo_a_ejecutar.py`

## Playlist Youtube
* [Curso de Flask - Codigo Facilito](https://www.youtube.com/watch?v=s6IF9ZVohz8&list=PLagErt3C7iltAydvN6SgCVKsOH4xQQKsk)
 